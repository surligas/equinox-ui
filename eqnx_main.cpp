#include "eqnx_main.h"
#include "ui_eqnx_main.h"

eqnx_main::eqnx_main(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::eqnx_main)
{
  ui->setupUi(this);
}

eqnx_main::~eqnx_main()
{
  delete ui;
}
