#include "eqnx_main.h"
#include <QApplication>
#include <QtSvg>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  eqnx_main w;
  w.show();

  return a.exec();
}
