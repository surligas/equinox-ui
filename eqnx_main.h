#ifndef EQNX_MAIN_H
#define EQNX_MAIN_H

#include <QMainWindow>

namespace Ui {
  class eqnx_main;
}

class eqnx_main : public QMainWindow
{
  Q_OBJECT

public:
  explicit eqnx_main(QWidget *parent = 0);
  ~eqnx_main();

private:
  Ui::eqnx_main *ui;
};

#endif // EQNX_MAIN_H
